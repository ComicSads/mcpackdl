// +build mage

package main

import (
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"path/filepath"
	"runtime"
)

// Aliases for functions
var Aliases = map[string]interface{}{
	"Cleanup": Clean,
	"Lt":      LoveTest,
}

// Name of executable
const Name = "mcpackdl"

// A function that builds an executable and runs it
func Test() error {
	mg.Deps(Build)
	err := sh.RunV(filepath.FromSlash("./"+Name), "-path=foo")
	if err != nil {
		return err
	}
	err = Clean()
	if err != nil {
		return err
	}
	return nil
}

// Builds the executable
func Build() error {
	err := sh.RunV("go", "build", "-o", filepath.FromSlash("./"+Name+exe(runtime.GOOS)))
	return err
}

// Builds executable for all operating systems
func BuildAll() error {
	systems := [3]string{"linux", "windows", "darwin"}
	for _, v := range systems {
		env := make(map[string]string)
		env["GOARCH"] = "386"
		env["GOOS"] = v
		name := Name + "-" + v + exe(v)
		err := sh.RunWithV(env, "go", "build", "-o", filepath.Join("build", name))
		if err != nil {
			return err
		}
	}
	return nil
}

// Removes built files
func Clean() error {
	err := sh.Rm(filepath.FromSlash("./" + Name + exe(runtime.GOOS)))
	if err != nil {
		return err
	}
	err = sh.Rm("foo")
	return err
}

// Builds executable then deletes it
// Used for finding errors in code
func LoveTest() error {
	// Build executable
	buildErr := Build()

	// Remove executable
	// Note that Clean wont return an error if the executable doesn't exist
	// Meaning that if Build can't create the executable, Clean still wont fail
	cleanErr := Clean()

	if cleanErr != nil {
		return cleanErr
	}
	if buildErr != nil {
		return buildErr
	}
	return nil
}

func exe(s string) string {
	switch s {
	case "windows":
		return ".exe"
	default:
		return ""
	}
}
