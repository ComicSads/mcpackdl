package main

import (
	"crypto/sha256"
	"flag"
	"fmt"
	"github.com/mitchellh/go-homedir"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

func exit(code int) {
	fmt.Scanln()
	os.Exit(code)
}

func main() {
	fmt.Fprintln(os.Stderr, "ComicSads Resource Pack Downloader V2.0")
	pathptr := flag.String("path", "your minecraft folder", "Directory to download Resource Pack")
	flag.Parse()

	var path string
	if *pathptr == "your minecraft folder" {
		var location string
		switch runtime.GOOS {
		case "darwin":
			fallthrough
		case "linux":
			s, err := homedir.Dir()
			if err != nil {
				fmt.Fprintln(os.Stderr, "Error finding home directory location:", err)
				exit(1)
			}
			location = s
		case "windows":
			s := os.Getenv("APPDATA")
			if s == "" {
				fmt.Fprintln(os.Stderr, "Unable to find location of \"APPDATA\" folder")
				exit(1)
			}
			location = s
		default:
			fmt.Fprintf(os.Stderr, "Unknown operating system %s! Aborting\n", runtime.GOOS)
			exit(1)
		}
		path = filepath.Join(location, ".minecraft", "resourcepacks")
	} else {
		path = *pathptr
	}

	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error, can't access minecraft directory:", err)
		exit(1)
	}
	fmt.Println("Downloading resource pack")
	err = Download(filepath.Join(path, "ComicSads.zip"), "https://comicsads.wtf/ResourcePack.zip")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		exit(1)
	}
	fmt.Println("Download complete. No errors!")
	exit(0)
}

var netClient = &http.Client{
	Timeout: time.Second * 30,
}

func Download(path, url string) error {
	fail := func(err error) error {
		return fmt.Errorf("Error downloading resource pack: %s", err.Error())
	}
	out, err := os.Create(path)
	if err != nil {
		return fail(err)
	}
	defer out.Close()

	res, err := netClient.Get(url)
	if err != nil {
		return fail(err)
	}
	defer res.Body.Close()

	_, err = io.Copy(out, res.Body)
	if err != nil {
		return fail(err)
	}
	return Verify(path)
}

// Verify SHA256sums
func Verify(path string) error {
	fail := func(err error) error {
		return fmt.Errorf("Error verifying downloaded pack: %s", err.Error())
	}
	f, err := os.Open(path)
	if err != nil {
		return fail(err)
	}
	defer f.Close()

	h := sha256.New()
	_, err = io.Copy(h, f)
	if err != nil {
		return fail(err)
	}

	res, err := netClient.Get("https://comicsads.wtf/ResourcePackSHA256")
	if err != nil {
		return fail(err)
	}
	defer res.Body.Close()

	sum, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fail(err)
	}
	a := string(sum)
	b := fmt.Sprintf("%x", h.Sum(nil))
	if a != b {
		return fmt.Errorf("SHA256sums don't match!\na:%s\nb:%s", a, b)
	}
	return nil
}
